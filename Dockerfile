FROM maven:3.8.2-jdk-11-slim

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN chmod +x ./wait-for-it.sh
RUN mvn package

CMD [ "sh", "-c", "mvn spring-boot:run" ]
