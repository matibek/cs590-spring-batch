# Spring Batch - Import Data from CSV to Mysql
This project uses spring-batch to import data from CSV file to mysql.

## How to run
```bash
$ docker-compose up
```

## Steps for testing
1. Create Admin user
```
curl --request POST 'localhost:8081/api/auth/signup' --header 'Content-Type: application/json' --data-raw '{
    "username": "admin",
    "password": "pass",
    "roles": ["ADMIN"]
}'
```
2. Login and get JWT token
```
curl --request POST 'localhost:8081/api/auth/login' --header 'Content-Type: application/json' --data-raw '{
    "username": "admin",
    "password": "pass"
}'
```
3. Invoke job as administrator (The job imports 5000 students to database)
```
curl --request POST 'localhost:8081/job/invoke' --header 'Authorization: Bearer {{token}}'
```
4. Get all imported students
```
curl --request GET 'localhost:8081/students' --header 'Authorization: Bearer {{token}}'
```