package edu.miu.cs590.springbatch.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.miu.cs590.springbatch.domain.Student;

@Configuration
@EnableBatchProcessing
public class BatchConfigration {
    @Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

    @Bean(name = "importJob")
	public Job job(@Qualifier("step1") Step step1) {
        return jobBuilderFactory.get("importJob")
        .start(step1)
        .incrementer(new RunIdIncrementer())
        .build();
    }

    @Bean
    public Step step1(StudentItemReader studentItemReader, StudentItemProcessor studentItemProcessor, StudentWriter studentWriter) {
        return stepBuilderFactory.get("step1")
        .<StudentItem, Student>chunk(10)
        .reader(studentItemReader)
        .processor(studentItemProcessor)
        .writer(studentWriter)
        .build();
    }
}
