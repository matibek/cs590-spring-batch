package edu.miu.cs590.springbatch.batch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentItem {
    private String firstName;
    private String lastName;
    private Double gpa;
    private Integer age;
}
