package edu.miu.cs590.springbatch.batch;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import edu.miu.cs590.springbatch.domain.Student;

@Component
public class StudentItemProcessor implements ItemProcessor<StudentItem, Student> {

	@Override
	public Student process(final StudentItem student) throws Exception {
		final Student transformedPerson = new Student();
		transformedPerson.setFirstName(student.getFirstName());
		transformedPerson.setLastName(student.getLastName());
		transformedPerson.setGpa(student.getGpa());
		transformedPerson.setDob(LocalDate.now().minusYears(student.getAge()).with(TemporalAdjusters.firstDayOfYear()));
		return transformedPerson;
	}

}
