package edu.miu.cs590.springbatch.batch;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class StudentItemReader extends FlatFileItemReader<StudentItem>{

    public StudentItemReader() {
        setName("studentItemReader");
        setResource(new ClassPathResource("sample-student-data.csv"));
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        String[] tokens = new String[]{"firstName", "lastName", "gpa", "age"};
        tokenizer.setNames(tokens);
        DefaultLineMapper<StudentItem> lineMapper = new DefaultLineMapper<StudentItem>();
        lineMapper.setLineTokenizer(tokenizer);
        lineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<StudentItem>() {{
            setTargetType(StudentItem.class);
        }});
        setLineMapper(lineMapper);
    }
}
