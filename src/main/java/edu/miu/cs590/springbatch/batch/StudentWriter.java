package edu.miu.cs590.springbatch.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.miu.cs590.springbatch.domain.Student;
import edu.miu.cs590.springbatch.repository.StudentRepository;

@Component
public class StudentWriter implements ItemWriter<Student>{

    @Autowired
	StudentRepository studentRepository ;
	
    @Override
    public void write(List<? extends Student> items) throws Exception {
        studentRepository.saveAll(items);
    }
    
}
