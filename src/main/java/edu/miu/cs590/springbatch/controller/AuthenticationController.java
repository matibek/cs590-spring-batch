package edu.miu.cs590.springbatch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.miu.cs590.springbatch.domain.User;
import edu.miu.cs590.springbatch.dto.LoginRequests;
import edu.miu.cs590.springbatch.dto.LoginResponse;
import edu.miu.cs590.springbatch.repository.UserRepository;
import edu.miu.cs590.springbatch.security.JwtTokenProvider;
import edu.miu.cs590.springbatch.security.UserPrincipal;

@RestController
@CrossOrigin
@RequestMapping("/api/auth/")
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginRequests loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new LoginResponse(jwt, (UserPrincipal) authentication.getPrincipal()));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> signup(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword())); // encrypt
        User newUser =  userRepository.save(user);
        return ResponseEntity.ok(UserPrincipal.create(newUser));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/check")
    public ResponseEntity<?> checkSecurity(Authentication authentication) {
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        User user = principal.getUser();
        String msg = String.format("Client Check Okay! Admin=%s User=%s", user.isAdmin(), user.isUser());
        return ResponseEntity.ok(msg);
    }
}