package edu.miu.cs590.springbatch.controller;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/job")
public class JobController {

    @Autowired
    @Qualifier("importJob")
    Job job;

    @Autowired
    JobLauncher jobLauncher;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping(value = "/invoke")
    public String invokeJob() throws Exception {
        System.out.println("Invoking job...");
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        JobExecution execution = jobLauncher.run(job, jobParameters);
        return execution.getStatus().toString();
    }
}
