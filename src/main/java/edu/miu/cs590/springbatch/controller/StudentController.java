package edu.miu.cs590.springbatch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.miu.cs590.springbatch.domain.Student;
import edu.miu.cs590.springbatch.repository.StudentRepository;

@RestController
@RequestMapping(value = "/students")
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    @GetMapping
    public Page<Student> getAll(Pageable pageable) {
        return studentRepository.findAll(pageable);
    }
}
