package edu.miu.cs590.springbatch.domain;

import java.util.Collection;
import javax.persistence.*;

import lombok.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {
    public enum Role {
        ADMIN, USER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String username;

    private String password;

    @ElementCollection
    private Collection<Role> roles;

    public boolean isAdmin() {
        return roles.stream().filter(r -> r.equals(Role.ADMIN)).findAny().isPresent();
    }

    public boolean isUser() {
        return roles.stream().filter(r -> r.equals(Role.USER)).findAny().isPresent();
    }
}
