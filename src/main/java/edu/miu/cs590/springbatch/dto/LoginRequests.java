package edu.miu.cs590.springbatch.dto;

import lombok.Data;

@Data
public class LoginRequests {

    private String username;

    private String password;

}
