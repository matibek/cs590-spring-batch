package edu.miu.cs590.springbatch.dto;

import edu.miu.cs590.springbatch.security.UserPrincipal;
import lombok.Data;

@Data
public class LoginResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private UserPrincipal user;

    public LoginResponse(String accessToken, UserPrincipal userDetails) {
        this.accessToken = accessToken;
        this.user = userDetails;
    }
}
