package edu.miu.cs590.springbatch.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import edu.miu.cs590.springbatch.domain.Student;

public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {
    
}
