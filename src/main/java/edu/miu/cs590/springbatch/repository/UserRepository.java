package edu.miu.cs590.springbatch.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.miu.cs590.springbatch.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
    public Optional<User> findOptionalByUsername(String username);
}
